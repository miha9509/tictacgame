// Размер стороны ячейки.
const sideSize = 40;
// Точка с которой начинается поле.
const fieldStartPoint = 10+0.5;
// На сколько пикселей фигура меньше ячейки.
const radiusCut = sideSize * 0.2;
const crossCut = sideSize * 0.2;
const sizeToWin = 3;

new Vue ({
  el: '#app',
  data: {
    size: 3,
    cellsNum: 9,
    c1: null,
    field: [],
    canvas: null,
    ctx: null,
    firstInit:true
  },
  mounted: function () {
    this.canvas = document.getElementById("c1");
    this.ctx = this.canvas.getContext("2d");
    // проверить сторадж
    const storage = getStorage();
    if (storage.field) {
      this.field = storage.field;
      this.size = storage.size;
      this.cellsNum = storage.cellsNum;
      this.drawField();
      this.drawAllFigures();
    } else {
      this.field = initArray(this.size);
      this.drawField();
    }
  },
  watch: {
    size: function (newSize, oldSize) {
      // Очистка поля.
      if (this.firstInit) {
        this.firstInit = false;
      } else {
        this.field = initArray(newSize);
        this.cellsNum = newSize * newSize;
        setStorage(newSize, this.cellsNum, this.field);
        this.canvas.width = this.canvas.width;
        this.drawField();
      }
    },
    cellsNum: function (newNums) {

    }
  },
  methods: {
    drawAllFigures () {
      if (this.field.length > 0) {
        for (var i = 0; i < this.size; i++) {
          for (var j = 0; j < this.size; j++) {
            if (this.field[i][j] == 2) {
              this.drawCircle(i, j);
            }
            if (this.field[i][j] == 1) {
              this.drawCross(i, j);
            }
          }
        }
      }
    },
    updateField () {
      if (this.cellsNum <= 0) {
        console.log('end of game');
        alert('Ничья');
        setStorage(null, null, null);
      }

      for (var i = 0; i < this.size; i++) {
        for (var j = 0; j < this.size; j++) {
          if (this.field.length > 0) {
            if (this.field[i][j] == 0) {
              this.field[i][j] = 2;
              this.cellsNum--;
              setStorage(this.size, this.cellsNum, this.field);
              this.checkWin(i, j);
              this.drawCircle(i, j);
              return;
            }
          }
        }
      }

      if (this.cellsNum <= 0) {
        console.log('end of game');
        alert('Ничья');
        setStorage(null, null, null);
      }
    },
    drawField () {
      // Отрисовка поля.
      for (var i = 0; i < this.size; i++) {
        for (var j = 0; j < this.size; j++) {
          this.ctx.rect(fieldStartPoint + (sideSize * i), fieldStartPoint + (sideSize * j), sideSize, sideSize);
        }
      }
      this.ctx.stroke();
    },
    canvasClick (e) {
      var mousePos = getMousePos(this.canvas, e);
      // Проверка выхода за границу поля.
      if (mousePos.x >= fieldStartPoint && mousePos.x <= fieldStartPoint + sideSize * this.size
          && mousePos.y >= fieldStartPoint && mousePos.y <= fieldStartPoint + sideSize * this.size){
        // Смотрим в какую клетку нажали.
        var pos = {
          x: Math.floor((mousePos.x - fieldStartPoint) / sideSize),
          y: Math.floor((mousePos.y - fieldStartPoint) / sideSize)
        };
        // Если клетка пустая, то ставим крестик.
        if (this.field[pos.x][pos.y] == 0) {
          this.field[pos.x][pos.y] = 1;
          this.cellsNum--;
          setStorage(this.size, this.cellsNum, this.field);
          this.drawCross(pos.x, pos.y);
          this.checkWin(pos.x, pos.y);
          this.updateField();
        }
      }
    },
    drawCross (x, y) {
      var startPoint = { 
        x: fieldStartPoint + sideSize * x,
        y: fieldStartPoint + sideSize * y
      };

      var endPoint = {
        x: startPoint.x + sideSize,
        y: startPoint.y + sideSize
      };

      this.ctx.beginPath();
      this.ctx.moveTo(startPoint.x + crossCut,startPoint.y + crossCut);
      this.ctx.lineTo(endPoint.x - crossCut, endPoint.y - crossCut);
      this.ctx.moveTo(endPoint.x - crossCut,startPoint.y + crossCut);
      this.ctx.lineTo(startPoint.x + crossCut,endPoint.y - crossCut);
      this.ctx.stroke();
      this.ctx.closePath();
    },
    drawCircle (x, y) {
      var centerPoint = { 
        x: fieldStartPoint + sideSize * x + Math.floor(sideSize / 2),
        y: fieldStartPoint + sideSize * y + Math.floor(sideSize / 2)
      };

      this.ctx.beginPath();
      this.ctx.arc(centerPoint.x, centerPoint.y, Math.floor(sideSize / 2) - radiusCut, 0, 2 * Math.PI, false);
      this.ctx.stroke();
      this.ctx.closePath();
    },
    checkWin (x, y) {
      const playerSign = this.field[x][y];
      // Проверим окрестности поставленой точки.
      var line = 0;
      var vertex = 0;
      // Горизонталь и вертикаль.
      // TODO: тоже ограничить число действий.
      for (var i = 0; i < this.size; i++) {
        if (this.field[i][y] == playerSign) {
          line++;
        } else {
          line = 0;
        }

        if (this.field[x][i] == playerSign) {
          vertex++;
        }
        else {
          vertex = 0;
        }

        if (line == sizeToWin || vertex == sizeToWin) {
          console.log('win player ', playerSign);
          alert('победил: ' + playerSign);
          //alert(playerSign);
          break;
        }
      }
      // Диагональ.
      var crossLine = 0;
      var operations = 0;
      const tmpX = x;
      const tmpY = y;

      while (x > 0 && y > 0 && operations < sizeToWin) {
        x--;
        y--;
        operations++;
      }

      operations = 0;

      while (x < this.size && y < this.size && operations < sizeToWin * 2) {
        if (this.field[x][y] == playerSign) {
          crossLine++;
        }
        else {
          crossLine = 0;
        }
        if (crossLine == sizeToWin) {
          console.log('win player ', playerSign);
          alert('победил: ' + playerSign);
          //alert(playerSign);
          break;
        }
        x++;
        y++;
        operations++;
      }
      // Побочная диагональ.
      crossLine = 0;
      operations = 0;
      x = tmpX;
      y = tmpY;
      while (x > 0 && y < this.size && operations < sizeToWin) {
        x--;
        y++;
        operations++;
      }

      operations = 0;
      while (x < this.size && y >= 0 && operations < sizeToWin * 2) {
        if (this.field[x][y] == playerSign) {
          crossLine++;
        }
        else {
          crossLine = 0;
        }
        if (crossLine == sizeToWin) {
          console.log('win player ', playerSign);
          alert('победил: ' + playerSign);
          //alert(playerSign);
          break;
        }
        x++;
        y--;
        operations++;
      }
    }
  }
});

function setStorage(size, cellsNum, field) {
  localStorage.setItem('size', size);
  localStorage.setItem('cellsNum', cellsNum);
  localStorage.setItem('field', field);
}

function getStorage() {
  if (!localStorage) { return null; }
  const size = localStorage.getItem('size') !== "null" ? localStorage.getItem('size') : null;
  const field = localStorage.getItem('field') !== "null" ? setArray(size, localStorage.getItem('field').split(',')) : null;
  const cellsNum = localStorage.getItem('cellsNum') !== "null" ? localStorage.getItem('cellsNum') : null;

  return {
    cellsNum: cellsNum,
    field: field,
    size: size
  };
}

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
  };
}

function initArray(size) {
  var arr = [];
  for (var i = 0; i < size; i++){
    arr[i] = [];
    for (var j = 0; j < size; j++){
      arr[i][j] = 0;
    }
  }

  return arr;
}

function setArray(size, array) {
  var arr = [];
  var counter = 0;

  for (var i = 0; i < size; i++){
    arr[i] = [];
    for (var j = 0; j < size; j++){
      arr[i][j] = array[counter];
      counter++;
    }
  }

  return arr;
}