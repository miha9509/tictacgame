package main

import (
	"encoding/json"
	"fmt"      // пакет для форматированного ввода вывода
	"log"      // пакет для логирования
	"net/http" // пакет для поддержки HTTP протокола
	"strconv"
	// пакет для работы с  UTF-8 строками
)

// SucessResponse - при успешном ответе
var SucessResponse = true

// FailResponse - при ошибке
var FailResponse = false

// CellNumbers - длинна поля в массиве
var CellNumbers = 8

// CellStart - начальная ячейка в массиве
var CellStart = 0

// MatrixSize - размерность матрицы
var MatrixSize = 3

type game struct {
	players int
	turn    int
	field   []int
	end     bool
}

// InitResponse структура ответа от сервера игроку при входе
type InitResponse struct {
	Success   bool
	Turn      int
	Field     []int
	End       bool
	PlayerNum int
}

// ErrorResponse структура сообщения с ошибкой
type ErrorResponse struct {
	Success bool
	Error   string
}

var currentGame *game

// addX handler
func addXHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if currentGame == nil {
		errorResponse("game not started", w)
		return
	}
	// если игрок не может сходить то ошибка
	if currentGame.turn%2 != 1 || currentGame.end || currentGame.players < 2 {
		errorResponse("end|1 player|false turn", w)
		return
	}
	err := setField(1, w, r)
	if err != nil {
		fmt.Println(err)
	}
}

// addY handler
func addYHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if currentGame == nil {
		errorResponse("game not started", w)
		return
	}
	// если игрок не может сходить то ошибка
	if currentGame.turn%2 == 1 || currentGame.end || currentGame.players < 2 {
		errorResponse("end|1 player|false turn", w)
		return
	}
	err := setField(2, w, r)
	if err != nil {
		fmt.Println(err)
	}
}

func setField(playerNum int, w http.ResponseWriter, r *http.Request) error {
	// если игрок может сходить, проверим не занята ли выбраная ячейка
	// получим ячейку, в которую ставится крестик
	cell, ok := r.URL.Query()["cell"]
	if !ok || len(cell[0]) < 1 {
		log.Println("Url Param 'key' is missing")
		return fmt.Errorf("Url Param 'key' is missing")
	}
	cellNumber, _ := strconv.Atoi(cell[0])
	// проверка возможности поставить в ячейку
	if cellNumber > CellNumbers || cellNumber < CellStart || currentGame.field[cellNumber] != 0 {
		errorResponse("cell not empty", w)
		return fmt.Errorf("can not put to this cell")
	}
	currentGame.field[cellNumber] = playerNum
	currentGame.turn++
	initResponse(playerNum, w)
	return nil
}

// addY main handler
func clearData(w http.ResponseWriter, r *http.Request) {
	currentGame = nil
}

// getInitData
func getInitData(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	// можно ли добавить игрока?
	if currentGame != nil {
		// уже играет 2 игрока
		if currentGame.players >= 2 {
			errorResponse("players limit", w)
			return
		}
		currentGame.players++
	}
	// если только подключился первый игрок
	if currentGame == nil {
		fmt.Println("init game")
		currentGame = &game{
			turn:    1,
			players: 1,
			end:     false,
			field:   []int{0, 0, 0, 0, 0, 0, 0, 0, 0},
		}
	}
	initResponse(currentGame.players, w)
}

func main() {
	http.HandleFunc("/init", getInitData) // запрос начальных данных
	http.HandleFunc("/clear", clearData)  // запрос чистки данных
	http.HandleFunc("/addX", addXHandler) // запрос от первого игрока
	http.HandleFunc("/addY", addYHandler) // запрос от второго игрока

	err := http.ListenAndServe("mygame:3000", nil) // задаем слушать порт
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
	fmt.Println("start listening server on localhost:9000", currentGame.turn)
}

// сообщение об ошибке
func errorResponse(message string, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	// готовим ответ от сервера
	response := &ErrorResponse{
		Success: FailResponse,
		Error:   message,
	}

	responseJSON, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(responseJSON)
}

func initResponse(player int, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	// готовим ответ от сервера
	response := InitResponse{
		Success:   SucessResponse,
		Turn:      currentGame.turn,
		Field:     currentGame.field,
		End:       currentGame.end,
		PlayerNum: player,
	}
	responseJSON, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(responseJSON)
}
